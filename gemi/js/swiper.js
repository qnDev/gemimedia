
var swiper = new Swiper('#header.swiper-container', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    spaceBetween: 100,
    centeredSlides: true,
    autoplay: 10000,
    autoplayDisableOnInteraction: false,
    loop: true,
    keyboardControl: true,
});

if (window.innerWidth<=480) {
        var swiper = new Swiper('#customer .swiper-container', {

        pagination: '.pagination-customer',
        slidesPerView:2,
        centeredSlides: true,
        paginationClickable: true,
        spaceBetween: 30,
        grabCursor: true,
        loop:true
    })

}else{
   var swiper = new Swiper('#customer .swiper-container', {

    pagination: '.pagination-customer',
    slidesPerView:5,
    centeredSlides: true,
    paginationClickable: true,
    spaceBetween: 30,
    grabCursor: true,
    loop:true
    })
}   


var swiper = new Swiper('#comment .swiper-container', {
    nextButton: '.swiper-button-next-comment',
    prevButton: '.swiper-button-prev-1',
    pagination: '.swiper-pagination',
    loop:true

});




if (window.innerWidth<=768 && window.innerWidth >480) {
    console.log('test');
    var swiper = new Swiper('#projects .swiper-container', {
        slidesPerView: 2,
        centeredSlides: true,
        paginationClickable: true,
        spaceBetween: 30,
        grabCursor: true,
        nextButton: '.swiper-button-next-2',
        prevButton: '.swiper-button-prev-2',
        loop:true,

        paginationClickable: true,
    })

}else if(window.innerWidth <= 480){
   var swiper = new Swiper('#projects .swiper-container', {
    slidesPerView: 1,
    centeredSlides: true,
    paginationClickable: true,
    spaceBetween: 30,
    grabCursor: true,
    nextButton: '.swiper-button-next-2',
    prevButton: '.swiper-button-prev-2',
    loop:true,
    paginationClickable: true,
})
}else{
     var swiper = new Swiper('#projects .swiper-container', {
    slidesPerView: 3,
    centeredSlides: true,
    paginationClickable: true,
    spaceBetween: 30,
    grabCursor: true,
    nextButton: '.swiper-button-next-2',
    prevButton: '.swiper-button-prev-2',
    loop:true,
    paginationClickable: true,
})}
